<?php

/* sociallogin.html */
class __TwigTemplate_7ce2f820c16c70b4623df7db39899209b0f5faa6f9e95a87b3044a092a4c39e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $asset_file = "@oneall_sociallogin/admin.js";
        $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
        if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
            $asset_path = $asset->get_path();            $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
            if (!file_exists($local_file)) {
                $local_file = $this->getEnvironment()->findTemplate($asset_path);
                $asset->set_path($local_file, true);
            }
            $asset->add_assets_version('43');
        }
        $this->getEnvironment()->get_assets_bag()->add_script($asset);        // line 3
        $asset_file = "@oneall_sociallogin/admin.css";
        $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
        if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
            $asset_path = $asset->get_path();            $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
            if (!file_exists($local_file)) {
                $local_file = $this->getEnvironment()->findTemplate($asset_path);
                $asset->set_path($local_file, true);
            }
            $asset->add_assets_version('43');
        }
        $this->getEnvironment()->get_assets_bag()->add_stylesheet($asset);        // line 4
        echo "
";
        // line 5
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "sociallogin.html", 5)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 6
        echo "
<script type=\"text/javascript\">
// <![CDATA[
    var OA_SOCIAL_LOGIN_AJAX_URL_AUTODETECT = '";
        // line 9
        echo (isset($context["OA_SOCIAL_LOGIN_AJAX_URL_AUTODETECT"]) ? $context["OA_SOCIAL_LOGIN_AJAX_URL_AUTODETECT"] : null);
        echo "';
    var OA_SOCIAL_LOGIN_AJAX_URL_VERIFY = '";
        // line 10
        echo (isset($context["OA_SOCIAL_LOGIN_AJAX_URL_VERIFY"]) ? $context["OA_SOCIAL_LOGIN_AJAX_URL_VERIFY"] : null);
        echo "';
//]]>
</script>

<div id=\"oneall_sociallogin\">
\t";
        // line 15
        if ((isset($context["OA_SOCIAL_LOGIN_SETTINGS_SAVED"]) ? $context["OA_SOCIAL_LOGIN_SETTINGS_SAVED"] : null)) {
            // line 16
            echo "\t\t<div class=\"successbox\">
\t\t\t<h3>Information</h3>
\t\t\t<p>";
            // line 18
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_SETTNGS_UPDATED");
            echo "</p>
\t\t</div>
\t";
        }
        // line 20
        echo "\t
\t";
        // line 21
        if ((isset($context["S_WARNING"]) ? $context["S_WARNING"] : null)) {
            // line 22
            echo "\t\t<div class=\"errorbox\">
\t   \t<h3>";
            // line 23
            echo $this->env->getExtension('phpbb\template\twig\extension')->lang("WARNING");
            echo "</h3>
\t   \t<p>";
            // line 24
            echo (isset($context["WARNING_MSG"]) ? $context["WARNING_MSG"] : null);
            echo "</p>
\t\t</div>
\t";
        }
        // line 27
        echo "\t<h1>";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_TITLE");
        echo "</h1>
\t<div class=\"section\">
\t\t<p>
\t\t\t";
        // line 30
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INTRO");
        echo "
\t\t</p>
\t</div>\t\t\t\t\t
\t<div class=\"section welcome\">
\t\t<h3>";
        // line 34
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_TITLE_HELP");
        echo "</h3>
\t\t<ul>
\t\t\t<li>";
        // line 36
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_FOLLOW_US_TWITTER");
        echo "</li>
\t\t\t<li>";
        // line 37
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_READ_DOCS");
        echo "</li>
\t\t\t<li>";
        // line 38
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DISCOVER_PLUGINS");
        echo "</li>
\t\t\t<li>";
        // line 39
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_GET_HELP");
        echo "</li>\t\t\t
\t\t</ul>
\t</div>
\t<div class=\"section get_started\">
\t\t<h4>";
        // line 43
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_CREATE_ACCOUNT_FIRST");
        echo "</h4>
\t\t<p>
\t\t\t";
        // line 45
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_SETUP_FREE_ACCOUNT");
        echo " | ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_VIEW_CREDENTIALS");
        echo "
\t\t</p>
\t</div>\t\t
\t<form id=\"oneall_social_login_form\" method=\"post\" action=\"";
        // line 48
        echo (isset($context["U_ACTION"]) ? $context["U_ACTION"] : null);
        echo "\">\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 50
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_ENABLE");
        echo "</legend>\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_disable_0\">";
        // line 53
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_ENABLE");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 54
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_ENABLE_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_disable_0\" name=\"oa_social_login_disable\" value=\"0\"";
        // line 57
        if ( !(isset($context["OA_SOCIAL_LOGIN_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_ENABLE_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label><br />
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_disable_1\" name=\"oa_social_login_disable\" value=\"1\"";
        // line 58
        if ((isset($context["OA_SOCIAL_LOGIN_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_ENABLE_NO");
        echo "</label>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t</fieldset>\t\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 63
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_CONNECTION");
        echo "</legend>
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_api_connection_handler_curl\">";
        // line 66
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 67
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_api_connection_handler_curl\" name=\"oa_social_login_api_connection_handler\" value=\"cr\"";
        // line 70
        if ((isset($context["OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER_CURL"]) ? $context["OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER_CURL"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_CURL");
        echo "</label><br />
\t\t\t\t\t<span>";
        // line 71
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_CURL_DESC");
        echo "</span> (";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_CURL_DOCS");
        echo ")<br /><br />\t\t\t\t\t
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_api_connection_handler_fsockopen\" name=\"oa_social_login_api_connection_handler\" value=\"fs\"";
        // line 72
        if ((isset($context["OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER_FSOCKOPEN"]) ? $context["OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER_FSOCKOPEN"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_FSOCKOPEN");
        echo "</label><br />
\t\t\t\t\t<span>";
        // line 73
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_FSOCKOPEN_DESC");
        echo "</span> (";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_FSOCKOPEN_DOCS");
        echo ")
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_api_connection_port_443\">";
        // line 78
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_PORT");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 79
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_PORT_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_api_connection_port_443\" name=\"oa_social_login_api_connection_port\" value=\"443\"";
        // line 82
        if ((isset($context["OA_SOCIAL_LOGIN_API_CONNECTION_PORT_443"]) ? $context["OA_SOCIAL_LOGIN_API_CONNECTION_PORT_443"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_PORT_443");
        echo "</label><br />
\t\t\t\t\t<span>";
        // line 83
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_PORT_443_DESC");
        echo "</span><br /><br />\t\t\t\t
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_api_connection_port_80\" name=\"oa_social_login_api_connection_port\" value=\"80\"";
        // line 84
        if ((isset($context["OA_SOCIAL_LOGIN_API_CONNECTION_PORT_80"]) ? $context["OA_SOCIAL_LOGIN_API_CONNECTION_PORT_80"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_PORT_80");
        echo "</label><br />
\t\t\t\t\t<span>";
        // line 85
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_PORT_80_DESC");
        echo "</span>\t\t\t\t
\t\t\t\t</dd>
\t\t\t</dl>\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<span id=\"oa_social_login_api_connection_handler_result\"></span>
\t\t\t\t</dt>
\t\t\t\t<dd> \t\t\t
\t\t\t\t\t<input class=\"button2\" type=\"button\" id=\"oa_social_login_autodetect_api_connection_handler\" value=\"";
        // line 93
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_AUTODETECT");
        echo "\" />               
\t\t\t\t</dd>
\t\t\t</dl>
\t\t</fieldset>\t\t\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 98
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_CREDENTIALS_TITLE");
        echo "</legend>\t\t
\t\t\t\t<dl>
\t\t\t\t\t<dt>
\t\t\t\t\t\t<label for=\"oa_social_login_api_subdomain\">";
        // line 101
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_SUBDOMAIN");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label>
\t\t\t\t\t</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_api_subdomain\" name=\"oa_social_login_api_subdomain\" value=\"";
        // line 104
        echo (isset($context["OA_SOCIAL_LOGIN_API_SUBDOMAIN"]) ? $context["OA_SOCIAL_LOGIN_API_SUBDOMAIN"] : null);
        echo "\"  size=\"60\" />
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<dl>
\t\t\t\t\t<dt>
\t\t\t\t\t\t<label for=\"oa_social_login_api_key\">";
        // line 109
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_PUBLIC_KEY");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label>
\t\t\t\t\t</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_api_key\" name=\"oa_social_login_api_key\" value=\"";
        // line 112
        echo (isset($context["OA_SOCIAL_LOGIN_API_KEY"]) ? $context["OA_SOCIAL_LOGIN_API_KEY"] : null);
        echo "\"  size=\"60\" />
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<dl>
\t\t\t\t\t<dt>
\t\t\t\t\t\t<label for=\"oa_social_login_api_secret\">";
        // line 117
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_PRIVATE_KEY");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label>
\t\t\t\t\t</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_api_secret\" name=\"oa_social_login_api_secret\" value=\"";
        // line 120
        echo (isset($context["OA_SOCIAL_LOGIN_API_SECRET"]) ? $context["OA_SOCIAL_LOGIN_API_SECRET"] : null);
        echo "\"  size=\"60\" />
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t\t\t<dl>
\t\t\t\t\t<dt>
\t\t\t\t\t\t<span id=\"oa_social_login_api_test_result\"></span>
\t\t\t\t\t</dt>
\t\t\t\t\t<dd> \t\t\t
\t\t\t\t\t\t<input class=\"button2\" type=\"button\" id=\"oa_social_login_test_api_settings\" value=\"";
        // line 128
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_API_VERIFY");
        echo "\" />               
\t\t\t\t\t</dd>
\t\t\t\t</dl>
\t\t</fieldset>\t\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 133
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DISPLAY_LOC");
        echo "</legend>\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_index_page_disable_0\">";
        // line 136
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INDEX_PAGE_ENABLE");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 137
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INDEX_PAGE_ENABLE_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_index_page_disable_1\" name=\"oa_social_login_index_page_disable\" value=\"1\"";
        // line 140
        if ((isset($context["OA_SOCIAL_LOGIN_INDEX_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_INDEX_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INDEX_PAGE_NO");
        echo "</label>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_index_page_disable_0\" name=\"oa_social_login_index_page_disable\" value=\"0\"";
        // line 141
        if ( !(isset($context["OA_SOCIAL_LOGIN_INDEX_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_INDEX_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INDEX_PAGE_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl class=\"dot\">
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_index_page_caption\">";
        // line 146
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INDEX_PAGE_CAPTION");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 147
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INDEX_PAGE_CAPTION_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_index_page_caption\" name=\"oa_social_login_index_page_caption\" value=\"";
        // line 150
        echo (isset($context["OA_SOCIAL_LOGIN_INDEX_PAGE_CAPTION"]) ? $context["OA_SOCIAL_LOGIN_INDEX_PAGE_CAPTION"] : null);
        echo "\" size=\"60\" />
\t\t\t\t</dd>
\t\t\t</dl>\t\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_login_page_disable_0\">";
        // line 155
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LOGIN_PAGE_ENABLE");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 156
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LOGIN_PAGE_ENABLE_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_login_page_disable_1\" name=\"oa_social_login_login_page_disable\" value=\"1\"";
        // line 159
        if ((isset($context["OA_SOCIAL_LOGIN_LOGIN_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_LOGIN_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LOGIN_PAGE_NO");
        echo "</label>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_login_page_disable_0\" name=\"oa_social_login_login_page_disable\" value=\"0\"";
        // line 160
        if ( !(isset($context["OA_SOCIAL_LOGIN_LOGIN_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_LOGIN_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LOGIN_PAGE_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl class=\"dot\">
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_login_page_caption\">";
        // line 165
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LOGIN_PAGE_CAPTION");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 166
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LOGIN_PAGE_CAPTION_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_login_page_caption\" name=\"oa_social_login_login_page_caption\" value=\"";
        // line 169
        echo (isset($context["OA_SOCIAL_LOGIN_LOGIN_PAGE_CAPTION"]) ? $context["OA_SOCIAL_LOGIN_LOGIN_PAGE_CAPTION"] : null);
        echo "\" size=\"60\" />
\t\t\t\t</dd>
\t\t\t</dl>\t\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_inline_page_disable_0\">";
        // line 174
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INLINE_PAGE_ENABLE");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 175
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INLINE_PAGE_ENABLE_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_inline_page_disable_1\" name=\"oa_social_login_inline_page_disable\" value=\"1\"";
        // line 178
        if ((isset($context["OA_SOCIAL_LOGIN_INLINE_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_INLINE_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INLINE_PAGE_NO");
        echo "</label>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_inline_page_disable_0\" name=\"oa_social_login_inline_page_disable\" value=\"0\"";
        // line 179
        if ( !(isset($context["OA_SOCIAL_LOGIN_INLINE_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_INLINE_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INLINE_PAGE_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl class=\"dot\">
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_inline_page_caption\">";
        // line 184
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INLINE_PAGE_CAPTION");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 185
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_INLINE_PAGE_CAPTION_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_inline_page_caption\" name=\"oa_social_login_inline_page_caption\" value=\"";
        // line 188
        echo (isset($context["OA_SOCIAL_LOGIN_INLINE_PAGE_CAPTION"]) ? $context["OA_SOCIAL_LOGIN_INLINE_PAGE_CAPTION"] : null);
        echo "\" size=\"60\" />
\t\t\t\t</dd>
\t\t\t</dl>\t\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_registration_page_disable_0\">";
        // line 193
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_REGISTRATION_PAGE_ENABLE");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 194
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_REGISTRATION_PAGE_ENABLE_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_registration_page_disable_1\" name=\"oa_social_login_registration_page_disable\" value=\"1\"";
        // line 197
        if ((isset($context["OA_SOCIAL_LOGIN_REGISTRATION_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_REGISTRATION_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_REGISTRATION_PAGE_NO");
        echo "</label>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_registration_page_disable_0\" name=\"oa_social_login_registration_page_disable\" value=\"0\"";
        // line 198
        if ( !(isset($context["OA_SOCIAL_LOGIN_REGISTRATION_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_REGISTRATION_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_REGISTRATION_PAGE_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label>\t\t\t\t\t
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl class=\"dot\">
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_registration_page_caption\">";
        // line 203
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_REGISTRATION_PAGE_CAPTION");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 204
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_REGISTRATION_PAGE_CAPTION_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_registration_page_caption\" name=\"oa_social_login_registration_page_caption\" value=\"";
        // line 207
        echo (isset($context["OA_SOCIAL_LOGIN_REGISTRATION_PAGE_CAPTION"]) ? $context["OA_SOCIAL_LOGIN_REGISTRATION_PAGE_CAPTION"] : null);
        echo "\" size=\"60\" />
\t\t\t\t</dd>
\t\t\t</dl>\t\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_other_page_disable_0\">";
        // line 212
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_OTHER_PAGE_ENABLE");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 213
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_OTHER_PAGE_ENABLE_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_other_page_disable_1\" name=\"oa_social_login_other_page_disable\" value=\"1\"";
        // line 216
        if ((isset($context["OA_SOCIAL_LOGIN_OTHER_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_OTHER_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_OTHER_PAGE_NO");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_other_page_disable_0\" name=\"oa_social_login_other_page_disable\" value=\"0\"";
        // line 217
        if ( !(isset($context["OA_SOCIAL_LOGIN_OTHER_PAGE_DISABLE"]) ? $context["OA_SOCIAL_LOGIN_OTHER_PAGE_DISABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_OTHER_PAGE_YES");
        echo "</label>\t\t\t\t
\t\t\t\t</dd>
\t\t\t</dl>
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_other_page_caption\">";
        // line 222
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_OTHER_PAGE_CAPTION");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 223
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_OTHER_PAGE_CAPTION_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_other_page_caption\" name=\"oa_social_login_other_page_caption\" value=\"";
        // line 226
        echo (isset($context["OA_SOCIAL_LOGIN_OTHER_PAGE_CAPTION"]) ? $context["OA_SOCIAL_LOGIN_OTHER_PAGE_CAPTION"] : null);
        echo "\" size=\"60\" />
\t\t\t\t</dd>
\t\t\t</dl>\t
\t\t</fieldset>\t\t\t\t\t\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 231
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_AVATARS");
        echo "</legend>\t\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_avatars_enable_0\">";
        // line 234
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_AVATARS");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 235
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_AVATARS_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_avatars_enable_0\" name=\"oa_social_login_avatars_enable\" value=\"0\"";
        // line 238
        if ( !(isset($context["OA_SOCIAL_LOGIN_AVATARS_ENABLE"]) ? $context["OA_SOCIAL_LOGIN_AVATARS_ENABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_AVATARS_ENABLE_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label><br />
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_avatars_enable_1\" name=\"oa_social_login_avatars_enable\" value=\"1\"";
        // line 239
        if ((isset($context["OA_SOCIAL_LOGIN_AVATARS_ENABLE"]) ? $context["OA_SOCIAL_LOGIN_AVATARS_ENABLE"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_AVATARS_ENABLE_NO");
        echo "</label>
\t\t\t\t</dd>
\t\t\t</dl>
\t\t</fieldset>\t\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 244
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_VALIDATION");
        echo "</legend>\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_validate_0\">";
        // line 247
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_VALIDATION_ASK");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 248
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_VALIDATION_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_validate_0\" name=\"oa_social_login_validate\" value=\"0\"";
        // line 251
        if (((isset($context["OA_SOCIAL_LOGIN_VALIDATE"]) ? $context["OA_SOCIAL_LOGIN_VALIDATE"] : null) == 0)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_VALIDATION_NEVER");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label><br />
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_validate_1\" name=\"oa_social_login_validate\" value=\"1\"";
        // line 252
        if (((isset($context["OA_SOCIAL_LOGIN_VALIDATE"]) ? $context["OA_SOCIAL_LOGIN_VALIDATE"] : null) == 1)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_VALIDATION_ALWAYS");
        echo "</label><br />\t
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_validate_2\" name=\"oa_social_login_validate\" value=\"2\"";
        // line 253
        if (((isset($context["OA_SOCIAL_LOGIN_VALIDATE"]) ? $context["OA_SOCIAL_LOGIN_VALIDATE"] : null) == 2)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_VALIDATION_DEPENDS");
        echo "</label>\t
\t\t\t\t</dd>
\t\t\t</dl>
\t\t</fieldset>
\t\t<fieldset>
\t\t\t<legend>";
        // line 258
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_LINKING");
        echo "</legend>\t\t
\t\t\t<dl>
\t\t\t\t<dt>
\t\t\t\t\t<label for=\"oa_social_login_disable_linking_0\">";
        // line 261
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_LINKING_ASK");
        echo "</label>
\t\t\t\t\t<br /><span>";
        // line 262
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_LINKING_DESC");
        echo "</span>
\t\t\t\t</dt>
\t\t\t\t<dd>
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_disable_linking_0\" name=\"oa_social_login_disable_linking\" value=\"0\"";
        // line 265
        if ( !(isset($context["OA_SOCIAL_LOGIN_DISABLE_LINKING"]) ? $context["OA_SOCIAL_LOGIN_DISABLE_LINKING"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_LINKING_YES");
        echo " <strong>(";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DEFAULT");
        echo ")</strong></label><br />
\t\t\t\t\t<label><input type=\"radio\" class=\"radio\" id=\"oa_social_login_disable_linking_1\" name=\"oa_social_login_disable_linking\" value=\"1\"";
        // line 266
        if ((isset($context["OA_SOCIAL_LOGIN_DISABLE_LINKING"]) ? $context["OA_SOCIAL_LOGIN_DISABLE_LINKING"] : null)) {
            echo " checked=\"checked\"";
        }
        echo " /> ";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_LINKING_NO");
        echo "</label>\t\t\t\t\t\t
\t\t\t\t</dd>\t\t\t
\t\t\t</dl>
\t\t</fieldset>\t\t
\t\t<fieldset>\t
\t\t\t\t<legend>";
        // line 271
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_REDIRECT");
        echo "</legend>\t\t
\t\t\t\t<dl>
\t\t\t\t\t<dt>
\t\t\t\t\t\t<label for=\"oa_social_login_redirect\">";
        // line 274
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_REDIRECT_ASK");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label>
\t\t\t\t\t\t<br /><span>";
        // line 275
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_DO_REDIRECT_DESC");
        echo "</span>
\t\t\t\t\t</dt>
\t\t\t\t\t<dd>
\t\t\t\t\t\t<input type=\"text\" id=\"oa_social_login_redirect\" name=\"oa_social_login_redirect\" value=\"";
        // line 278
        echo (isset($context["OA_SOCIAL_LOGIN_REDIRECT"]) ? $context["OA_SOCIAL_LOGIN_REDIRECT"] : null);
        echo "\" size=\"60\" />
\t\t\t\t\t</dd>
\t\t\t\t</dl>\t
\t\t</fieldset>\t\t
\t\t<fieldset>
\t\t\t<legend>";
        // line 283
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_ENABLE_NETWORKS");
        echo "</legend>\t\t\t\t\t\t
\t\t\t";
        // line 284
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["loops"]) ? $context["loops"] : null), "provider", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["provider"]) {
            // line 285
            echo "\t\t\t\t<dl>\t
\t\t\t\t\t<dt class=\"provider_box\">\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<label for=\"oa_social_login_provider_";
            // line 287
            echo $this->getAttribute($context["provider"], "KEY", array());
            echo "\"><span class=\"oa_social_login_provider oa_social_login_provider_";
            echo $this->getAttribute($context["provider"], "KEY", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["provider"], "NAME", array());
            echo "\">";
            echo $this->getAttribute($context["provider"], "NAME", array());
            echo "</span></label>\t\t\t\t\t\t
\t\t\t\t\t\t\t<input style=\"cursor:pointer\" type=\"checkbox\" id=\"oa_social_login_provider_";
            // line 288
            echo $this->getAttribute($context["provider"], "KEY", array());
            echo "\" name=\"oa_social_login_provider_";
            echo $this->getAttribute($context["provider"], "KEY", array());
            echo "\" value=\"1\" ";
            if ($this->getAttribute($context["provider"], "ENABLE", array())) {
                echo " checked=\"checked\"";
            }
            echo "  />
\t\t\t\t\t\t\t<label for=\"oa_social_login_provider_";
            // line 289
            echo $this->getAttribute($context["provider"], "KEY", array());
            echo "\">";
            echo $this->getAttribute($context["provider"], "NAME", array());
            echo "</label>
\t\t\t\t\t</dt>
\t\t\t\t\t<dd></dd>
\t\t\t\t</dl>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['provider'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 293
        echo "\t\t\t
\t\t</fieldset>\t\t
\t\t<fieldset class=\"submit-buttons\">
\t\t\t<input class=\"button1\" type=\"submit\" id=\"submit\" name=\"submit\" value=\"";
        // line 296
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("SUBMIT");
        echo "\" />
\t\t\t";
        // line 297
        echo (isset($context["S_FORM_TOKEN"]) ? $context["S_FORM_TOKEN"] : null);
        echo "
\t\t</fieldset>
\t</form>
</div>
";
        // line 301
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "sociallogin.html", 301)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
    }

    public function getTemplateName()
    {
        return "sociallogin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  825 => 301,  818 => 297,  814 => 296,  809 => 293,  796 => 289,  786 => 288,  776 => 287,  772 => 285,  768 => 284,  764 => 283,  756 => 278,  750 => 275,  745 => 274,  739 => 271,  727 => 266,  717 => 265,  711 => 262,  707 => 261,  701 => 258,  689 => 253,  681 => 252,  671 => 251,  665 => 248,  661 => 247,  655 => 244,  643 => 239,  633 => 238,  627 => 235,  623 => 234,  617 => 231,  609 => 226,  603 => 223,  599 => 222,  587 => 217,  577 => 216,  571 => 213,  567 => 212,  559 => 207,  553 => 204,  549 => 203,  535 => 198,  527 => 197,  521 => 194,  517 => 193,  509 => 188,  503 => 185,  499 => 184,  485 => 179,  477 => 178,  471 => 175,  467 => 174,  459 => 169,  453 => 166,  449 => 165,  435 => 160,  427 => 159,  421 => 156,  417 => 155,  409 => 150,  403 => 147,  399 => 146,  385 => 141,  377 => 140,  371 => 137,  367 => 136,  361 => 133,  353 => 128,  342 => 120,  335 => 117,  327 => 112,  320 => 109,  312 => 104,  305 => 101,  299 => 98,  291 => 93,  280 => 85,  272 => 84,  268 => 83,  260 => 82,  254 => 79,  249 => 78,  239 => 73,  231 => 72,  225 => 71,  217 => 70,  211 => 67,  206 => 66,  200 => 63,  188 => 58,  178 => 57,  172 => 54,  168 => 53,  162 => 50,  157 => 48,  149 => 45,  144 => 43,  137 => 39,  133 => 38,  129 => 37,  125 => 36,  120 => 34,  113 => 30,  106 => 27,  100 => 24,  96 => 23,  93 => 22,  91 => 21,  88 => 20,  82 => 18,  78 => 16,  76 => 15,  68 => 10,  64 => 9,  59 => 6,  47 => 5,  44 => 4,  33 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "sociallogin.html", "");
    }
}
