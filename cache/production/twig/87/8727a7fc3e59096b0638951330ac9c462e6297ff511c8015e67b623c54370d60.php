<?php

/* user_welcome_inactive.txt */
class __TwigTemplate_69256de20de69b2854fce087bf03e5824a2b26acd16b4a923f4618068f22740c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Subject: フォーラムサイト \"";
        echo (isset($context["SITENAME"]) ? $context["SITENAME"] : null);
        echo "\" へようこそ - ";
        echo (isset($context["U_BOARD"]) ? $context["U_BOARD"] : null);
        echo "

";
        // line 3
        echo (isset($context["WELCOME_MSG"]) ? $context["WELCOME_MSG"] : null);
        echo "

このメールは大切に保管してください。
あなたのアカウント情報はこちらです：

----------------------------
ユーザー名： ";
        // line 9
        echo (isset($context["USERNAME"]) ? $context["USERNAME"] : null);
        echo "
----------------------------

こちらをクリックしてアカウントを有効化してください：

";
        // line 14
        echo (isset($context["U_ACTIVATE"]) ? $context["U_ACTIVATE"] : null);
        echo "

パスワードはデータベース内に暗号化された状態で保管されます。
暗号化されたパスワードを元の状態に復元する事はできません。
その為もしパスワードを忘れてしまった場合、そのパスワードは二度と取り戻す事ができない点にご注意ください。
万が一パスワードを忘れてしまった場合、
アカウントのメールアドレスを使用してパスワードを再発行できます。

ご登録ありがとうございました。

";
        // line 24
        echo (isset($context["EMAIL_SIG"]) ? $context["EMAIL_SIG"] : null);
    }

    public function getTemplateName()
    {
        return "user_welcome_inactive.txt";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 24,  44 => 14,  36 => 9,  27 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "user_welcome_inactive.txt", "");
    }
}
