<?php

/* @oneall_sociallogin/auth_provider_oneall.html */
class __TwigTemplate_28ef4532d9af1596697631fe1acca6204225bb11765ac2dd46bb9917f30bf247 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<fieldset id=\"auth_oneall_settings\">
";
        // line 2
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_ACP_AUTH_SETTTING_INFO");
        echo "
</fieldset>
";
    }

    public function getTemplateName()
    {
        return "@oneall_sociallogin/auth_provider_oneall.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@oneall_sociallogin/auth_provider_oneall.html", "");
    }
}
