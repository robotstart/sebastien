<?php

/* sociallogin_ucp_social_link.html */
class __TwigTemplate_b1cfe131f1a9be819199b1854d7614210ef39d1b1ee365762fc2f132d00aee9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $location = "ucp_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("ucp_header.html", "sociallogin_ucp_social_link.html", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "<h2>";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LINK");
        echo "</h2>

<div class=\"panel\">
\t<div class=\"inner\">
\t\t<p>";
        // line 6
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LINK_DESC1");
        echo "<br />";
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LINK_DESC2");
        echo "</p>
\t\t<p class=\"error\">";
        // line 7
        echo (isset($context["OA_SOCIAL_LINK_ERROR"]) ? $context["OA_SOCIAL_LINK_ERROR"] : null);
        echo "</p>
\t\t<p><strong>";
        // line 8
        echo (isset($context["OA_SOCIAL_LINK_SUCCESS"]) ? $context["OA_SOCIAL_LINK_SUCCESS"] : null);
        echo "</strong></p>
\t\t<dl>
\t\t\t<dt>
\t\t\t\t<label>";
        // line 11
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LINK_NETWORKS");
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("COLON");
        echo "</label><br>
\t\t\t\t<span>";
        // line 12
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("OA_SOCIAL_LOGIN_LINK_ACTION");
        echo "</span><br /><br />
\t\t\t</dt>
\t\t</dl>
\t\t<dl>
\t\t\t<dt>
\t\t\t\t<div class=\"oneall_social_login\" id=\"oneall_social_login_link_profile\"></div>
\t\t\t</dt>
\t\t</dl>
\t</div>
</div>

<!-- OneAll Social Login : http://www.oneall.com //-->
<script type=\"text/javascript\">
\t// <![CDATA[\t\t\t\t\t            
\t\tvar _oneall = _oneall || [];
\t\t_oneall.push(['social_link', 'set_providers', ['";
        // line 27
        echo (isset($context["OA_SOCIAL_LOGIN_PROVIDERS"]) ? $context["OA_SOCIAL_LOGIN_PROVIDERS"] : null);
        echo "']]);
\t\t_oneall.push(['social_link', 'set_user_token', '";
        // line 28
        echo (isset($context["OA_SOCIAL_LINK_USER_TOKEN"]) ? $context["OA_SOCIAL_LINK_USER_TOKEN"] : null);
        echo "']);
\t\t_oneall.push(['social_link', 'set_callback_uri', '";
        // line 29
        echo (isset($context["OA_SOCIAL_LINK_CALLBACK_URI"]) ? $context["OA_SOCIAL_LINK_CALLBACK_URI"] : null);
        echo "']);
\t\t_oneall.push(['social_link', 'do_render_ui', 'oneall_social_login_link_profile']);
\t// ]]>
</script>
\t\t\t\t

";
        // line 35
        $location = "ucp_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("ucp_footer.html", "sociallogin_ucp_social_link.html", 35)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
    }

    public function getTemplateName()
    {
        return "sociallogin_ucp_social_link.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 35,  86 => 29,  82 => 28,  78 => 27,  60 => 12,  55 => 11,  49 => 8,  45 => 7,  39 => 6,  31 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "sociallogin_ucp_social_link.html", "");
    }
}
