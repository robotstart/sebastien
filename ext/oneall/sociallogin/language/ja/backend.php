<?php
/**
 * @package   	OneAll Social Login
 * @copyright 	Copyright 2013-2015 http://www.oneall.com - All rights reserved.
 * @license   	GNU/GPL 2 or later
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 */
if (! defined ('IN_PHPBB'))
{
	exit ();
}

if (empty ($lang) || ! is_array ($lang))
{
	$lang = array ();
}

// Social Login Backend.
$lang = array_merge ($lang, array (
	'OA_SOCIAL_LOGIN_PAGE_CAPTION' => 'ソーシャルアカウントでログイン',
	'OA_SOCIAL_LOGIN_ACP' => 'ソーシャルログイン',
	'OA_SOCIAL_LOGIN_ACP_SETTINGS' => '設定',
	'OA_SOCIAL_LOGIN_API_AUTODETECT' => 'オートAPIの接続を検出',
	'OA_SOCIAL_LOGIN_API_CONNECTION' => 'API接続',
	'OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER' => 'APIの接続ハンドラ',
	'OA_SOCIAL_LOGIN_API_CONNECTION_HANDLER_DESC' => 'OneAllは、ソーシャルメディアのAPIへのコネクションマネージャです',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_CHECK_COM' => 'APIに接続できませんでした。 APIの接続設定が正しくありますか？',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_FILL_OUT' => '上記の各フィールドに必要事項を記入してください。',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_KEYS_WRONG' => 'API資格は、公開鍵/秘密鍵を確認してください、間違っています.',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_OK' => '設定が正しいか - 変更内容を保存することを忘れないでください！',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_SUBDOMAIN_WRONG' => 'サブドメインが存在しません。あなたはそれを正しく記入しましたか？',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_TITLE' => 'APIの資格証明 - <a href="https://app.oneall.com/applications/" class="external">作成するか、またはあなたのAPIの資格情報を表示するにはここをクリックします</a>',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_UNKNOW_ERROR' => '不明な応答 - あなたがログインしていることを確認してください！',
	'OA_SOCIAL_LOGIN_API_CREDENTIALS_USE_AUTO' => '接続ハンドラは動作していないよう。自動検出を使用してください。',
	'OA_SOCIAL_LOGIN_API_DETECT_CURL' => 'ポート %s の上で検出されたCURL - 変更内容を保存することを忘れないでください！',
	'OA_SOCIAL_LOGIN_API_DETECT_FSOCKOPEN' => 'ポートで検出すぐにわかるの %s - 変更内容を保存することを忘れないでください！',
	'OA_SOCIAL_LOGIN_API_DETECT_NONE' => 'Connection failed! Your firewall must allow outbound request on either port 80 or 443.',
	'OA_SOCIAL_LOGIN_API_PORT' => 'APIの接続ポート',
	'OA_SOCIAL_LOGIN_API_PORT_DESC' => 'ファイアウォールは、ポート80または443のいずれかに発信要求を許可する必要があります。',
	'OA_SOCIAL_LOGIN_API_PRIVATE_KEY' => 'API秘密鍵(Private)',
	'OA_SOCIAL_LOGIN_API_PUBLIC_KEY' => 'API公開鍵(Public)',
	'OA_SOCIAL_LOGIN_API_SUBDOMAIN' => 'APIのサブドメイン',
	'OA_SOCIAL_LOGIN_API_VERIFY' => 'APIの設定を確認します',
	'OA_SOCIAL_LOGIN_CREATE_ACCOUNT_FIRST' => 'ソーシャルログインを使用できるようにするには、アカウント登録が必要です。<a href="https://app.oneall.com/signup/" class="external">http://www.oneall.com</a>でセットアップしてください。',
	'OA_SOCIAL_LOGIN_CURL' => 'PHP CURL',
	'OA_SOCIAL_LOGIN_CURL_DESC' => 'CURLを使用することをお勧めしますが、いくつかのサーバで無効になっている可能性があります。',
	'OA_SOCIAL_LOGIN_CURL_DOCS' => '<a href="http://www.php.net/manual/en/book.curl.php" class="external">CURLマニュアル</a>',
	'OA_SOCIAL_LOGIN_DEFAULT' => 'デフォルト',
	'OA_SOCIAL_LOGIN_DISCOVER_PLUGINS' => 'Drupal, Joomla, WordPressのプラグインを<a href="http://docs.oneall.com/plugins/" class="external">発見する</a>',
	'OA_SOCIAL_LOGIN_DISPLAY_LOC' => 'どこの社会ログインを表示したいですか？',
	'OA_SOCIAL_LOGIN_DO_AVATARS' => 'ソーシャルネットワークからアップロードするアバターを有効にしますか？',
	'OA_SOCIAL_LOGIN_DO_AVATARS_DESC' => '彼のソーシャルネットワークプロファイルからユーザーのアバターを取得し、あなたのphpBBのアバターフォルダに格納できるようにします。',
	'OA_SOCIAL_LOGIN_DO_AVATARS_ENABLE_NO' => 'いいえ、ソーシャルネットワークのアバターを使用していません',
	'OA_SOCIAL_LOGIN_DO_AVATARS_ENABLE_YES' => 'はい、ソーシャルネットワークのアバターを使用',
	'OA_SOCIAL_LOGIN_DO_ENABLE' => 'ソーシャルログインを有効にしますか？',
	'OA_SOCIAL_LOGIN_DO_ENABLE_DESC' => 'あなたは一時的にそれを削除することなく、ソーシャルログインを無効にすることができます。',
	'OA_SOCIAL_LOGIN_DO_ENABLE_NO' => '無効にします',
	'OA_SOCIAL_LOGIN_DO_ENABLE_YES' => '有効にします',
	'OA_SOCIAL_LOGIN_DO_LINKING' => 'ソーシャルネットワークのアカウントのリンクを有効にしますか？',
	'OA_SOCIAL_LOGIN_DO_LINKING_ASK' => '自動的にソーシャルネットワークは、ユーザアカウントを既存のにアカウントのリンク？',
	'OA_SOCIAL_LOGIN_DO_LINKING_DESC' => '有効にした場合、検証済みのメールアドレスを持つソーシャルネットワークアカウントは、同じメールアドレスを持つ既存のphpBBのユーザーアカウントにリンクされます。',
	'OA_SOCIAL_LOGIN_DO_LINKING_NO' => '無効にするアカウントのリンク',
	'OA_SOCIAL_LOGIN_DO_LINKING_YES' => 'アカウントのリンクを有効にします',
	'OA_SOCIAL_LOGIN_DO_REDIRECT' => 'リダイレクション',
	'OA_SOCIAL_LOGIN_DO_REDIRECT_ASK' => 'このページにユーザーをリダイレクトしました。彼らはソーシャルネットワークのアカウントで接続しました。',
	'OA_SOCIAL_LOGIN_DO_REDIRECT_DESC' => 'あなたのphpBBのページへの完全なURLを入力します。空のままにした場合、ユーザーは同じページに留まります。',
	'OA_SOCIAL_LOGIN_DO_VALIDATION' => '新しいユーザーのプロファイルのプロンプト検証？',
	'OA_SOCIAL_LOGIN_DO_VALIDATION_ALWAYS' => 'プロファイルの検証を有効にします',
	'OA_SOCIAL_LOGIN_DO_VALIDATION_ASK' => 'プロンプトの新規ユーザーは、ユーザー名とメールアドレスを検証するには？',
	'OA_SOCIAL_LOGIN_DO_VALIDATION_DEPENDS' => '例外的プロファイル検証',
	'OA_SOCIAL_LOGIN_DO_VALIDATION_DESC' => '有効にした場合、新しいユーザーが完了するか、ユーザー名とメールアドレスを確認するように求められます。選択した場合<br/>例外的な検証はユーザ名が取られたときに、メールを送信メールが取られるが、自動ソーシャルリンクが無効になっている、不足しているに発生します。',
	'OA_SOCIAL_LOGIN_DO_VALIDATION_NEVER' => '無効にするプロファイル検証',
	'OA_SOCIAL_LOGIN_ENABLE_NETWORKS' => 'あなたのフォーラム上で有効にするためにソーシャルネットワークを選択します。',
	'OA_SOCIAL_LOGIN_ENABLE_SOCIAL_NETWORK' => 'あなたは、少なくとも一つの社会的ネットワークを有効にする必要があります',
	'OA_SOCIAL_LOGIN_ENTER_CREDENTIALS' => 'あなたのAPIの資格セットアップする必要があります。',
	'OA_SOCIAL_LOGIN_FOLLOW_US_TWITTER' => 'Twitterで更新に関する情報があります。<a href="http://www.twitter.com/oneall" class="external">@oneall</a>;',
	'OA_SOCIAL_LOGIN_FSOCKOPEN' => 'PHPすぐにわかります',
	'OA_SOCIAL_LOGIN_FSOCKOPEN_DESC' => 'あなたはCURLで問題が発生した場合にのみ、すぐにわかるを使用しています。',
	'OA_SOCIAL_LOGIN_FSOCKOPEN_DOCS' => '<a href="http://www.php.net/manual/en/function.fsockopen.php" class="external">すぐにわかるマニュアル</a>',
	'OA_SOCIAL_LOGIN_GET_HELP' => '意見等がありましたら<a href="http://www.oneall.com/company/contact-us/" class="external">お問い合わせ</a>ください!',
	'OA_SOCIAL_LOGIN_INDEX_PAGE' => 'フォーラムホームページ',
	'OA_SOCIAL_LOGIN_INDEX_PAGE_CAPTION' => 'メインページのキャプション',
	'OA_SOCIAL_LOGIN_INDEX_PAGE_CAPTION_DESC' => 'このタイトルは、メインページ上のソーシャルログインアイコンの上に表示されます。',
	'OA_SOCIAL_LOGIN_INDEX_PAGE_ENABLE' => 'メインページに表示？',
	'OA_SOCIAL_LOGIN_INDEX_PAGE_ENABLE_DESC' => '有効にした場合、社会的なログインがメインページに表示されます。',
	'OA_SOCIAL_LOGIN_INDEX_PAGE_NO' => 'いいえ',
	'OA_SOCIAL_LOGIN_INDEX_PAGE_YES' => 'はい、メインページに表示',
	'OA_SOCIAL_LOGIN_INTRO' => 'あなたの訪問者がログインし、とりわけTwitterやFacebook、LinkedInの、Hyves、VKontakte、GoogleやYahooなどのソーシャルネットワークへの登録を許可します。ソーシャルログインの<strong>登録プロセスを簡略化することで、ユーザー登録率</strong>を高め、ソーシャルネットワークのプロフィールから取り出さ許可ベースのソーシャルの<strong>データ</strong>を提供します。あなたとあなたのユーザードンはゼロからスタートしなければならないので、ソーシャルログインは、既存の登録システムと統合されています。',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE' => 'フォーラムログインページ',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE_CAPTION' => 'ログインページのキャプション',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE_CAPTION_DESC' => 'このタイトルは、ログイン・ページ上のソーシャルログインアイコンの上に表示されます.',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE_ENABLE' => 'ログインページに表示しますか？',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE_ENABLE_DESC' => '有効にした場合、社会的なログインは、ログインページに表示されます。',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE_NO' => 'いいえ',
	'OA_SOCIAL_LOGIN_LOGIN_PAGE_YES' => 'はい、ログインページに表示',
	'OA_SOCIAL_LOGIN_OTHER_PAGE' => '他のページ',
	'OA_SOCIAL_LOGIN_OTHER_PAGE_CAPTION' => '他のページのキャプション',
	'OA_SOCIAL_LOGIN_OTHER_PAGE_CAPTION_DESC' => 'このタイトルは、他のページにソーシャルログインアイコンの上に表示されます。',
	'OA_SOCIAL_LOGIN_OTHER_PAGE_ENABLE' => '他のページに表示されますか？',
	'OA_SOCIAL_LOGIN_OTHER_PAGE_ENABLE_DESC' => '有効にした場合、社会的なログインはまた、フォーラムの他のページに表示されます。',
	'OA_SOCIAL_LOGIN_OTHER_PAGE_NO' => 'いいえ',
	'OA_SOCIAL_LOGIN_OTHER_PAGE_YES' => 'はい、他のページに表示',
	'OA_SOCIAL_LOGIN_PORT_443' => 'ポート443上のHTTPS経由の通信',
	'OA_SOCIAL_LOGIN_PORT_443_DESC' => 'ポート443をお勧めします使用していますが、サーバーにOpenSSLをインストールする必要があります。',
	'OA_SOCIAL_LOGIN_PORT_80' => 'ポート80上のHTTP経由の通信',
	'OA_SOCIAL_LOGIN_PORT_80_DESC' => '少し速くポート80をされている使用して、OpenSSLを必要とするが、あまり安全ではありません。',
	'OA_SOCIAL_LOGIN_PROFILE_DESC' => 'ソーシャルネットワークにあなたのアカウントをリンク',
	'OA_SOCIAL_LOGIN_PROFILE_TITLE' => 'ソーシャルログイン',
	'OA_SOCIAL_LOGIN_READ_DOCS' => '<a href="http://docs.oneall.com/plugins/" class="external">このプラグインの詳細については、オンラインマニュアルを参照して</a>を読みます;',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE' => 'フォーラムの登録ページ',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE_CAPTION' => '登録ページのキャプション',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE_CAPTION_DESC' => 'このタイトルは、登録ページにソーシャルログインアイコンの上に表示されます。',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE_ENABLE' => '登録ページに表示？',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE_ENABLE_DESC' => '有効にした場合、社会的なログインは登録ページに表示されます。',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE_NO' => 'いいえ',
	'OA_SOCIAL_LOGIN_REGISTRATION_PAGE_YES' => 'はい、登録ページに表示',
	'OA_SOCIAL_LOGIN_SETTINGS' => '設定',
	'OA_SOCIAL_LOGIN_SETTNGS_UPDATED' => '設定が正常に更新します。',
	'OA_SOCIAL_LOGIN_SETUP_FREE_ACCOUNT' => '<a href="https://app.oneall.com/signup/" class="button1 external">セットアップ無料アカウント</a>',
	'OA_SOCIAL_LOGIN_SOCIAL_LINK' => 'ソーシャルリンクサービス',
	'OA_SOCIAL_LOGIN_TITLE' => 'OneAllソーシャルログイン',
	'OA_SOCIAL_LOGIN_TITLE_HELP' => 'ヘルプ、アップデート ドキュメンテーション',
	'OA_SOCIAL_LOGIN_VALIDATION_FORM_DESC' => '管理者は、あなたが確認またはユーザー名とメールアドレスを完了することが必要です。',
	'OA_SOCIAL_LOGIN_VALIDATION_FORM_EMAIL_EXPLAIN' => '受け入れるか、メールアドレスを変更します。',
	'OA_SOCIAL_LOGIN_VALIDATION_FORM_EMAIL_NONE_EXPLAIN' => 'メールアドレスがあなたのプロフィールから欠落している、それをここに記入してください。',
	'OA_SOCIAL_LOGIN_VALIDATION_FORM_HEADER' => 'ユーザー名とメールアドレスを検証します',
	'OA_SOCIAL_LOGIN_VALIDATION_SESSION_ERROR' => 'セッション情報が欠落しています。',
	'OA_SOCIAL_LOGIN_VIEW_CREDENTIALS' => '<a href="https://app.oneall.com/applications/" class="button1 external">アカウントを作成する</a>',
	'OA_SOCIAL_LOGIN_WIDGET_TITLE' => 'ソーシャルネットワークを使用してログイン',
));
