<?php
/**
 * @package   	OneAll Social Login
 * @copyright 	Copyright 2013-2015 http://www.oneall.com - All rights reserved.
 * @license   	GNU/GPL 2 or later
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 */
if (! defined ('IN_PHPBB'))
{
	exit ();
}

if (empty ($lang) || ! is_array ($lang))
{
	$lang = array ();
}

// Social Login Frontend.
$lang = array_merge ($lang, array (
	'OA_SOCIAL_LOGIN_PAGE_CAPTION' => 'ソーシャルアカウントでログイン',
	'OA_SOCIAL_LOGIN_LINK_UCP' => 'ソーシャルネットワークのアカウントをリンク',
	'OA_SOCIAL_LOGIN_LINK' => 'ソーシャルネットワークのアカウントをリンク',
	'OA_SOCIAL_LOGIN_LINK_NETWORKS' => 'ソーシャルネットワーク',
	'OA_SOCIAL_LOGIN_LINK_DESC1' => 'このページでは、あなたのソーシャルネットワークがあなたのフォーラムのアカウントにアカウントを接続することができます。',
	'OA_SOCIAL_LOGIN_LINK_DESC2' => 'ソーシャルネットワークのアカウントを接続した後、あなたはまた、フォーラムにログインするために使用することができます。',
	'OA_SOCIAL_LOGIN_LINK_ACTION' => 'リンクする/リンク解除はソーシャルネットワークのアイコンをクリックしてください。',
	'OA_SOCIAL_LOGIN_ENABLE_SOCIAL_NETWORK' => 'あなたは、少なくとも一つの社会的ネットワークを有効にする必要があります',
	'OA_SOCIAL_LOGIN_ENTER_CREDENTIALS' => 'あなたのAPIの資格セットアップする必要があります',
	'OA_SOCIAL_LOGIN_SOCIAL_LINK' => 'ソーシャルリンクサービス',
	'OA_SOCIAL_LOGIN_ACCOUNT_ALREADY_LINKED' => 'このソーシャルネットワークのアカウントは既に別のフォーラムのユーザーにリンクされています。',
	'OA_SOCIAL_LOGIN_ACCOUNT_INACTIVE_OTHER' => 'アカウントが作成されました。しかし、フォーラムの設定は、アカウントの有効化が必要になります。<br />有効化するリンクはあなたのメールアドレスに送信されました。',
	'OA_SOCIAL_LOGIN_ACCOUNT_INACTIVE_ADMIN' => 'アカウントが作成されました。しかし、フォーラムの設定は、管理者によるアカウントの有効化を必要とする。<br />電子メールが管理者に送信され、アカウントが有効にされた後、あなたは電子メールで通知されます',
));
